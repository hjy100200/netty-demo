package com.hjy.study.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hjy on 2018/6/1.
 */
@Controller
public class MainController {
    @RequestMapping("/socketio_2")
    public String login() {
        return "socketio_2";
    }

    @RequestMapping("/socketio_namespace")
    public String namespace() {
        return "socketio_namespace";
    }
}
