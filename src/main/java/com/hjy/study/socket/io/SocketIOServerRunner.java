package com.hjy.study.socket.io;

import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by hjy on 2018/6/4.
 */
@Component
public class SocketIOServerRunner implements CommandLineRunner {
    private final SocketIOServer server;


    @Autowired
    public SocketIOServerRunner(SocketIOServer server) {
        this.server = server;
    }

    /**
     * 必须在此启动socket.io
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        server.start();
        System.out.println("=====================Socket.io server is start!=========================");
    }
}
