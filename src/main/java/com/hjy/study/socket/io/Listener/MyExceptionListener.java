package com.hjy.study.socket.io.Listener;

import com.corundumstudio.socketio.listener.ExceptionListenerAdapter;
import io.netty.channel.ChannelHandlerContext;

/**
 * socket.io的ie浏览器刷新页面，会报异常处理
 * Created by hjy on 2018/6/6.
 */
public class MyExceptionListener extends ExceptionListenerAdapter {
    @Override
    public boolean exceptionCaught(ChannelHandlerContext ctx, Throwable e) {
        System.out.println(e.getMessage());
        ctx.close();

        return true;
    }
}