package com.hjy.study.socket.io.Handler;

import java.util.*;

import com.corundumstudio.socketio.SocketIONamespace;
import com.hjy.study.socket.io.message.MessageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;


/**
 * 消息处理类 OnConnect和OnDisconnect整个项目只能有一个，多个不会报错，是无用功
 * Created by hjy on 2018/5/31.
 */
@Component
public class MessageEventHandler {
    private final SocketIOServer server;
    static Set<UUID> listClient = new HashSet<>();

    @Autowired
    public MessageEventHandler(SocketIOServer server) {
        this.server = server;
    }

    //添加connect事件，当客户端发起连接时调用，本文中将clientid与sessionid存入数据库
    //方便后面发送消息时查找到对应的目标client,
    @OnConnect
    public void onConnect(SocketIOClient client) {
        if(client != null){
            String clientId = client.getHandshakeData().getSingleUrlParam("clientid");
            listClient.add(client.getSessionId());
            System.out.println("客户端 :" + client.getSessionId() + "已连接"+clientId);
        }else{
            System.out.println("================没有客户端连接=================");
        }

    }

    //添加@OnDisconnect事件，客户端断开连接时调用，刷新客户端信息
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        String clientId = client.getHandshakeData().getSingleUrlParam("clientid");
        System.out.println("客户端:" + client.getSessionId() + "断开连接"+clientId);
    }

    /**
     * 发送到数据到客户端，使用client.getSessionId()来表示具体某个客户端
     * @param client
     * @param request
     * @param data
     */
    //消息接收入口，当接收到消息后，查找发送目标客户端，并且向该客户端发送消息，且给自己发送消息
    @OnEvent(value = "messageevent")
    public void onEvent(SocketIOClient client, AckRequest request, MessageInfo data) {
        String targetClientId = data.getTargetClientId();
        System.out.println(targetClientId+"客户端发来消息：" + data.getMsgContent());

        for(UUID clientId : listClient){
            MessageInfo sendData = new MessageInfo();
            sendData.setSourceClientId(data.getSourceClientId());
            sendData.setTargetClientId(data.getTargetClientId());
            sendData.setMsgType("chat");
            sendData.setMsgContent(data.getMsgContent());

            server.getClient(clientId).sendEvent("messageevent", sendData);
        }
    }

    @OnEvent(value = "namespace")
    public void getNamespace(SocketIOClient ioClient, AckRequest request,MessageInfo data){
        System.out.println("房间namespace发来消息：" + data.getMsgContent());

        ioClient.joinRoom("namespace");

        MessageInfo sendData = new MessageInfo();
        sendData.setSourceClientId(data.getSourceClientId());
        sendData.setTargetClientId(data.getTargetClientId());
        sendData.setMsgType("房间消息");
        sendData.setMsgContent(data.getMsgContent());

        server.getRoomOperations("namespace").sendEvent("namespace",sendData);

    }


}